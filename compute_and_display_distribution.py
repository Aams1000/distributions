import matplotlib.pyplot as plt
import seaborn as sns
from typing import *
import csv
import sys
from datetime import datetime
from enum import Enum


class FundCategory(Enum):
    LARGE_CAP = "Large cap"
    MID_CAP = "Mid cap"
    SMALL_CAP = "Small cap"
    INTERNATIONAL = "International"
    INTERNATIONAL_EXPLORER = "International explorer"
    INDIVIDUAL_STOCK = "Individual stocks"
    CASH = "Cash"


_FUND_NAMES_TO_TYPE: Dict[str, FundCategory] = {"NTDOY": FundCategory.INDIVIDUAL_STOCK,
                                                "TWTR": FundCategory.INDIVIDUAL_STOCK,
                                                "SCHV": FundCategory.LARGE_CAP,
                                                "SFILX": FundCategory.INTERNATIONAL_EXPLORER,
                                                "SWISX": FundCategory.INTERNATIONAL,
                                                "SWPPX": FundCategory.LARGE_CAP,
                                                "SWSSX": FundCategory.SMALL_CAP,
                                                "SWTSX": FundCategory.LARGE_CAP,
                                                "SWMCX": FundCategory.MID_CAP,
                                                "VINEX": FundCategory.INTERNATIONAL_EXPLORER,
                                                "VSMAX": FundCategory.SMALL_CAP,
                                                "Cash & Cash Investments": FundCategory.CASH}


def plot(data: List[float], labels: List[str]) -> None:
    #define Seaborn color palette to use
    colors = sns.color_palette('pastel')

    #create pie chart
    plt.pie(data, labels = labels, colors = colors, autopct='%.0f%%')
    date: str = datetime.now().strftime('%Y-%m-%d')
    plt.title(f"Brokerage account investment distribution {date}")
    plt.savefig(f"./plots/{date}.png")
    plt.show()


def read_and_process_data(path: str) -> Tuple[List[float], List[str]]:
    total_account_value: float = 0.0
    values_by_category: Dict[str, float] = {}
    with open(path) as csvfile:
        rows = csv.reader(csvfile, delimiter=',')
        next(rows)
        for row in rows:
            fund_name: str = row[0]
            if fund_name in _FUND_NAMES_TO_TYPE:
                value: float = float(row[6][1:].replace(",", ""))
                total_account_value += value
                fund_category: str = _FUND_NAMES_TO_TYPE[fund_name].value
                values_by_category[fund_category] = values_by_category.get(fund_category, 0.0) + value
            else:
                print(f"Skipping row with fund name '{fund_name}")

    for category, value in values_by_category.items():
        values_by_category[category] = round((value / total_account_value) * 100)
    print(f"Total account value: {total_account_value}")
    for category, value in values_by_category.items():
        print(f"{category}: {value}%")
    return values_by_category.values(), values_by_category.keys()


def compute_and_display_distribution(path: str) -> None:
    values_and_labels: Tuple[List[float], List[str]] = read_and_process_data(path)
    plot(values_and_labels[0], values_and_labels[1])


if __name__ == "__main__":
    compute_and_display_distribution(sys.argv[1])
